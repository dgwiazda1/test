# TeaCode React.js Recruitment Challenge

## To run this project:

```
$ npm install
$ npm start
```

now You can use app on http://localhost:3000


## Technologies
App is created with:
* React.js
* Axios
* Bootstrap
* Styled-components


