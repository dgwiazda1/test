import React, {useState, useEffect} from "react";

import styled from "styled-components";

import 'bootstrap/dist/css/bootstrap.min.css';
import {
    Table, Form, Button
} from "react-bootstrap";
import ContactService from "./contact.service";

const Styles = styled.div`
  header {
    display: flex;
    align-items: center;
    justify-content: center;
    color: white;
    background-color: #179B6D;
  }
  
  #search-form {
    display: flex;
    
    .btn {
      width: 300px;
    }
  }
  
  .table {
    td {
      display: flex;
      align-items: center;
    }
    img {
      border-radius: 50%;
      width: 50px;
      height: 50px;
      margin-right: 20px;
    }
  }
`;

function App() {
    const [contacts, setContacts] = useState([]);
    const [allContacts, setAllContacts] = useState([]);
    const [checkList, setCheckList] = useState([]);
    const [checkListChanged, setCheckListChanged] = useState(0);
    const [searchSentence, setSearchSentence] = useState("");

    const compare = (a, b) => {
        if (a.last_name < b.last_name) {
            return -1;
        }
        if (a.last_name > b.last_name) {
            return 1;
        }
        return 0;
    }

    useEffect(() => {
        ContactService.getContacts().then((response) => {
            setAllContacts((response.data).sort(compare));
            setContacts((response.data).sort(compare));
        })
        console.log("no");
    }, [checkListChanged]);

    const onCheckboxToggle = (id) => {
        var index = checkList.indexOf(parseInt(id, 10));
        var tempCheckList = checkList;
        if (checkList.includes(id)) {
            if (index !== -1) {
                tempCheckList.splice(index, 1);
            }
        } else {
            tempCheckList.push(id);
        }
        setCheckList(tempCheckList);
        checkList.sort();
        if (checkListChanged > 10) {
            setCheckListChanged(0);
        } else {
            setCheckListChanged(checkListChanged + 1);
        }
        console.log(checkList);
    }

    const onChangeSearch = (e) => {
        const search = e.target.value;
        setSearchSentence(search);
    };

    const searching = () => {
        var contactsTemp = [];
        allContacts.forEach(function (c) {
            var lastname = c.last_name;
            var firstname = c.first_name;
            if (lastname.includes(searchSentence) || firstname.includes(searchSentence)) {
                contactsTemp.push(c);
                console.log(c);
            }
        });
        setContacts(contactsTemp);
    }

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            searching();
        }
    }
    return (
        <Styles>
            <header>
                <h1>Contacts</h1>
            </header>
            <div id="search-form">
                <Form.Control type="text" placeholder="Search CASE SENSITIVE!" onChange={onChangeSearch}
                              onKeyDown={handleKeyDown}/>
                <Button variant="info" onClick={searching}>Search</Button>
            </div>
            <Table striped bordered>
                <tbody>
                {contacts.map((contact) => (
                    <tr key={contact.id}>
                        <td onClick={() => {
                            onCheckboxToggle(contact.id)
                        }} style={{opacity: checkList.includes(contact.id) ? 0.7 : 1}}>
                            <img src={contact.avatar}
                                 alt={contact.avatar}/>
                            <h4>{contact.first_name} {contact.last_name}</h4>
                        </td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </Styles>
    );
}

export default App;
