import axios from "axios";

const API_URL = "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json";

class ContactService {
    getContacts() {
        return axios.get(API_URL);
    }
}

export default new ContactService();